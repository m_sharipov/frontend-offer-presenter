# `Библиотека для работы с офферами`

Пример использования

```javascript
const offerPresenter = new OfferPresenter({ category: 'flatSale' });

if (offerPresenter.isCommercial()) {
  // do something
}
```