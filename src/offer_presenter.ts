import { FDealType, FOfferType, IOfferPresenterData } from './__types__';
import { getDealType } from './utils/get_deal_type';
import { getType } from './utils/get_type';

export class OfferPresenter {
  /**
   * Тип сделки
   */
  public dealType: FDealType;
  /**
   * Тип оффера
   */
  public type: FOfferType;

  public constructor(data: IOfferPresenterData) {
    this.dealType = getDealType(data);
    this.type = getType(data);
  }

  /**
   * Городская недвижимость
   */
  public isUrban(): boolean {
    return (this.type & FOfferType.Urban) !== 0;
  }

  /**
   * Загородная недвижимость
   */
  public isSuburban(): boolean {
    return (this.type & FOfferType.Suburban) !== 0;
  }

  /**
   * Жилая недвижимость
   */
  public isResidential(): boolean {
    return (this.type & FOfferType.Residential) !== 0;
  }

  /**
   * Коммерческая недвижимость
   */
  public isCommercial(): boolean {
    return (this.type & FOfferType.Commercial) !== 0;
  }

  /**
   * Квартира в новостройке и во вторичке
   */
  public isFlat(): boolean {
    return (this.type & FOfferType.Flat) !== 0;
  }

  /**
   * Загородная жилая недвижимость
   */
  public isSuburbanResidential(): boolean {
    return (this.type & (FOfferType.Suburban & ~FOfferType.Land)) !== 0;
  }

  /**
   * Аренда длительная и посуточная
   */
  public isRent(): boolean {
    return (this.dealType & FDealType.Rent) !== 0;
  }

}
