import { OfferPresenter } from '../offer_presenter';
import { offerMock } from './offer.mock';

describe('Offer presenter public methods', () => {

  it('Is urban', () => {
    const offerPresenter = new OfferPresenter({
      ...offerMock,
      category: 'flatRent',
    });

    expect(offerPresenter.isUrban()).toBeTruthy();
  });

  it('Is suburban', () => {
    const offerPresenter = new OfferPresenter({
      ...offerMock,
      category: 'houseSale',
    });

    expect(offerPresenter.isSuburban()).toBeTruthy();
  });

  it('Is residential', () => {
    const offerPresenter = new OfferPresenter({
      ...offerMock,
      category: 'townhouseRent',
    });

    expect(offerPresenter.isResidential()).toBeTruthy();
  });

  it('Is commercial', () => {
    const offerPresenter = new OfferPresenter({
      ...offerMock,
      category: 'commercialLandSale',
    });

    expect(offerPresenter.isCommercial()).toBeTruthy();
  });

  it('Is flat', () => {
    const offerPresenter = new OfferPresenter({
      ...offerMock,
      category: 'flatSale',
    });

    expect(offerPresenter.isFlat()).toBeTruthy();
  });

  it('Is suburban residential', () => {
    const offerPresenter = new OfferPresenter({
      ...offerMock,
      category: 'houseShareSale',
    });

    expect(offerPresenter.isSuburbanResidential()).toBeTruthy();
  });

  it('Is rent', () => {
    const offerPresenter = new OfferPresenter({
      ...offerMock,
      category: 'flatRent',
    });

    expect(offerPresenter.isRent()).toBeTruthy();
  });

});
