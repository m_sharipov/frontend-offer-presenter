import 'tslib';

import { FOfferType, IOfferPresenterData, TOfferCategory } from '../../__types__';
import { getType } from '../../utils/get_type';
import { offerMock } from '../offer.mock';

describe('Get offer type', () => {

  it('Unexpected for invalid category', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'invalidValue' as TOfferCategory,
    };

    expect(getType(offer)).toEqual(FOfferType.Unexpected);
  });

  it('FlatOld', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'flatRent',
    };

    expect(getType(offer)).toEqual(FOfferType.FlatOld);
  });

  it('FlatNew', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'newBuildingFlatSale',
    };

    expect(getType(offer)).toEqual(FOfferType.FlatNew);
  });

  it('FlatShared', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'flatShareSale',
    };

    expect(getType(offer)).toEqual(FOfferType.FlatShared);
  });

  it('Room', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'roomSale',
    };

    expect(getType(offer)).toEqual(FOfferType.Room);
  });

  it('Bed', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'bedRent',
    };

    expect(getType(offer)).toEqual(FOfferType.Bed);
  });

  it('House', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'houseRent',
    };

    expect(getType(offer)).toEqual(FOfferType.House);
  });

  it('HousePart', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'houseShareRent',
    };

    expect(getType(offer)).toEqual(FOfferType.HousePart);
  });

  it('Cottage', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'cottageSale',
    };

    expect(getType(offer)).toEqual(FOfferType.Cottage);
  });

  it('Townhouse', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'townhouseRent',
    };

    expect(getType(offer)).toEqual(FOfferType.Townhouse);
  });

  it('Land', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'landSale',
    };

    expect(getType(offer)).toEqual(FOfferType.Land);
  });

  it('CommercialLand', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'commercialLandSale',
    };

    expect(getType(offer)).toEqual(FOfferType.CommercialLand);
  });

  it('Office', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'officeRent',
    };

    expect(getType(offer)).toEqual(FOfferType.Office);
  });

  it('ShoppingArea', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'shoppingAreaSale',
    };

    expect(getType(offer)).toEqual(FOfferType.TradeArea);
  });

  it('Warehouse', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'warehouseSale',
    };

    expect(getType(offer)).toEqual(FOfferType.Warehouse);
  });

  it('FreeAppointmentObject', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'freeAppointmentObjectSale',
    };

    expect(getType(offer)).toEqual(FOfferType.FreeAppointmentObject);
  });

  it('PublicCatering', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'publicCateringRent',
    };

    expect(getType(offer)).toEqual(FOfferType.PublicCatering);
  });

  it('Garage', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'garageRent',
    };

    expect(getType(offer)).toEqual(FOfferType.Garage);
  });

  it('Manufacture', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'industrySale',
    };

    expect(getType(offer)).toEqual(FOfferType.Manufacture);
  });

  it('AutoService', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'carServiceSale',
    };

    expect(getType(offer)).toEqual(FOfferType.AutoService);
  });

  it('Business', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'businessRent',
    };

    expect(getType(offer)).toEqual(FOfferType.Business);
  });

  it('Building', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'buildingSale',
    };

    expect(getType(offer)).toEqual(FOfferType.Building);
  });

  it('DomesticServices', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'domesticServicesRent',
    };

    expect(getType(offer)).toEqual(FOfferType.DomesticServices);
  });

});
