import { FDealType, IOfferPresenterData, TOfferCategory } from '../../__types__';
import { getDealType } from '../../utils/get_deal_type';
import { offerMock } from '../offer.mock';

describe('Get deal type', () => {

  it('Unexpected for invalid category', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'invalidValue' as TOfferCategory,
    };

    expect(getDealType(offer)).toEqual(FDealType.Unexpected);
  });

  it('RentDaily', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'dailyBedRent',
    };

    expect(getDealType(offer)).toEqual(FDealType.RentDaily);
  });

  it('RentLongterm', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'flatRent',
    };

    expect(getDealType(offer)).toEqual(FDealType.RentLongterm);
  });

  it('Sale', () => {
    const offer: IOfferPresenterData = {
      ...offerMock,
      category: 'houseSale',
    };

    expect(getDealType(offer)).toEqual(FDealType.Sale);
  });

});
