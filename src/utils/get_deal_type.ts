import { FDealType, IOfferPresenterData } from '../__types__';

export function getDealType(offer: IOfferPresenterData): FDealType {
  const category = offer.category.toLowerCase();

  if (category.includes('daily')) {
    return FDealType.RentDaily;
  }

  if (category.includes('rent')) {
    return FDealType.RentLongterm;
  }

  if (category.includes('sale')) {
    return FDealType.Sale;
  }

  return FDealType.Unexpected;
}
