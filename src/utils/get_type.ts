import { FOfferType, IOfferPresenterData } from '../__types__';

export function getType(offer: IOfferPresenterData): FOfferType {
  const { category } = offer;

  if (category.startsWith('bed')) {
    return FOfferType.Bed;
  }

  if (category.startsWith('room')) {
    return FOfferType.Room;
  }

  if (category.startsWith('flat')) {
    if (category.startsWith('flatShare')) {
      return FOfferType.FlatShared;
    }

    return FOfferType.FlatOld;
  }

  if (category.startsWith('newBuildingFlat')) {
    return FOfferType.FlatNew;
  }

  if (category.startsWith('house')) {
    if (category.startsWith('houseShare')) {
      return FOfferType.HousePart;
    }

    return FOfferType.House;
  }

  if (category.startsWith('cottage')) {
    return FOfferType.Cottage;
  }

  if (category.startsWith('townhouse')) {
    return FOfferType.Townhouse;
  }

  if (category.startsWith('warehouse')) {
    return FOfferType.Warehouse;
  }

  if (category.startsWith('land')) {
    return FOfferType.Land;
  }

  if (category.startsWith('commercialLand')) {
    return FOfferType.CommercialLand;
  }

  if (category.startsWith('office')) {
    return FOfferType.Office;
  }

  if (category.startsWith('shoppingArea')) {
    return FOfferType.TradeArea;
  }

  if (category.startsWith('freeAppointmentObject')) {
    return FOfferType.FreeAppointmentObject;
  }

  if (category.startsWith('publicCatering')) {
    return FOfferType.PublicCatering;
  }

  if (category.startsWith('garage')) {
    return FOfferType.Garage;
  }

  if (category.startsWith('industry')) {
    return FOfferType.Manufacture;
  }

  if (category.startsWith('carService')) {
    return FOfferType.AutoService;
  }

  if (category.startsWith('business')) {
    return FOfferType.Business;
  }

  if (category.startsWith('building')) {
    return FOfferType.Building;
  }

  if (category.startsWith('domesticServices')) {
    return FOfferType.DomesticServices;
  }

  return FOfferType.Unexpected;
}
