import { TOfferCategory } from './offer/category';

export * from './offer/category';
export * from './offer/deal_type';
export * from './offer/type';

export interface IOfferPresenterData {
  category: TOfferCategory;
}
