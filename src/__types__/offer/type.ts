export const enum FOfferType {
  /**
   * Не использовать
   */
  Unexpected            = 0,
  /**
   * Квартира во вторичке
   */
  FlatOld               = 1 << 1,
  /**
   * Квартира в новостройке
   */
  FlatNew               = 1 << 2,
  /**
   * Квартира
   */
  Flat                  = FlatOld | FlatNew,
  /**
   * Доля
   */
  FlatShared            = 1 << 3,
  /**
   * Комната
   */
  Room                  = 1 << 4,
  /**
   * Койко-место
   */
  Bed                   = 1 << 5,
  /**
   * Жилая городская
   */
  Urban                 = Flat | FlatShared | Room | Bed,
  /**
   * Дом/дача
   */
  House                 = 1 << 6,
  /**
   * Часть дома
   */
  HousePart             = 1 << 7,
  /**
   * Коттедж
   */
  Cottage               = 1 << 8,
  /**
   * Таунхаус
   */
  Townhouse             = 1 << 9,
  /**
   * Участок
   */
  Land                  = 1 << 10,
  /**
   * Загородная
   */
  Suburban              = House | HousePart | Cottage | Townhouse | Land,
  /**
   * Жилая
   */
  Residential           = Urban | Suburban,
  /**
   * Офис
   */
  Office                = 1 << 11,
  /**
   * Торговая площадь
   */
  TradeArea             = 1 << 12,
  /**
   * Склад
   */
  Warehouse             = 1 << 13,
  /**
   * Помещение свободного назначения
   */
  FreeAppointmentObject = 1 << 14,
  /**
   * Общепит
   * @deprecated
   * @description Нужно использовать возможное назначение
   */
  PublicCatering        = 1 << 15,
  /**
   * Гараж
   */
  Garage                = 1 << 16,
  /**
   * Производство
   */
  Manufacture           = 1 << 17,
  /**
   * Автосервис
   * @deprecated
   * @description Нужно использовать возможное назначение
   */
  AutoService           = 1 << 18,
  /**
   * Готовый бизнес
   */
  Business              = 1 << 19,
  /**
   * Здание
   */
  Building              = 1 << 20,
  /**
   * Бытовые услуги
   * @deprecated
   * @description Нужно использовать возможное назначение
   */
  DomesticServices      = 1 << 21,
  /**
   * Коммерческая земля
   */
  CommercialLand        = 1 << 22,
  /**
   * Коммерческая
   */
  Commercial            = Office | TradeArea | Warehouse |
                          FreeAppointmentObject | PublicCatering | Garage |
                          Manufacture | AutoService | Business |
                          Building | DomesticServices | CommercialLand,
  Any                   = Residential | Commercial,
}
