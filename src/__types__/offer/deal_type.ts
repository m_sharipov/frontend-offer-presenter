export const enum FDealType {
  /**
   * Не использовать
   */
  Unexpected   = 0,
  /**
   * Продажа
   */
  Sale         = 1 << 0,
  /**
   * Долгосрочная аренда
   */
  RentLongterm = 1 << 1,
  /**
   * Посуточная аренда
   */
  RentDaily    = 1 << 2,
  /**
   * Аренда
   */
  Rent         = RentLongterm | RentDaily,
  /**
   * Любой тип сделки
   */
  Any          = Sale | Rent,
}
