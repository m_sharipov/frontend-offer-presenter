export type TOfferCategory =
// Койко-место
'bedRent' |
'dailyBedRent' |
// Здание
'buildingRent' |
'buildingSale' |
// Бизнес
'businessRent' |
'businessSale' |
// Автосервис (deprecated)
'carServiceRent' |
'carServiceSale' |
// Коммерческая земля
'commercialLandRent' |
'commercialLandSale' |
// Коттедж
'cottageRent' |
'cottageSale' |
// Бытовые услуги
'domesticServicesRent' |
'domesticServicesSale' |
// Квартира
'flatRent' |
'flatSale' |
'dailyFlatRent' |
// Доля в квартире
'flatShareSale' |
// Помещение свободного назначения
'freeAppointmentObjectRent' |
'freeAppointmentObjectSale' |
// Гараж
'garageRent' |
'garageSale' |
// Дом
'houseRent' |
'houseSale' |
'dailyHouseRent' |
// Часть дома
'houseShareRent' |
'houseShareSale' |
// Производство
'industryRent' |
'industrySale' |
// Земля
'landSale' |
// Квартира в новостройке
'newBuildingFlatSale' |
// Офис
'officeRent' |
'officeSale' |
// Общепит (deprecated)
'publicCateringRent' |
'publicCateringSale' |
// Комната
'roomRent' |
'roomSale' |
'dailyRoomRent' |
// Торговое помещение (deprecated)
'shoppingAreaRent' |
'shoppingAreaSale' |
// Таунхаус
'townhouseRent' |
'townhouseSale' |
// Склад
'warehouseRent' |
'warehouseSale';
